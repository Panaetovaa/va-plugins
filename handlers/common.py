from jmm import models


def build_response(iterable):
    label__points = {}

    for dt, label, value in iterable:
        label__points.setdefault(label, [])
        print(dt, label, value)
        label__points[label].append(
            models.Point(
                value=value,
                timestamp=dt.timestamp(),
            )
        )

    return label__points


def get_issues_in_sprints(sprints, issues):
    issues_in_sprints = {s.name: set(s.issues) for s in sprints}

    for issue in issues:
        for c in issue.changes:
            if c.field == "Sprint":
                for s in c.value_to_string.split(","):
                    if s:
                        issues_in_sprints.setdefault(s.strip(), set([]))
                        issues_in_sprints[s.strip()].add(issue.key)

    return {s: list(issues) for s, issues in issues_in_sprints.items()}


def is_done(issue, from_, to_):
    for change in issue.changes:
        if (
            change.field == "status"
            and change.value_to_string in TERMINATED_STATUSES
        ):
            if from_ <= change.created <= to_:
                return True
            else:
                return False

    return False


IN_PROGRESS_STATUS = "In progress"
OPEN_STATUS = "Open"
TESTING_STATUS = "Testing"
READY_FOR_TEST_STATUS = "Ready for Test"
CLOSED_STATUS = "Closed"
READY_TO_CHECK_STATUS = "Ready to Check"
READY_TO_INSTALL_STATUS = "Ready to Install"
DONE_STATUS = "Done"

TERMINATED_STATUSES = [
    CLOSED_STATUS,
    READY_TO_CHECK_STATUS,
    READY_TO_INSTALL_STATUS,
    DONE_STATUS,
]
