from typing import Dict, List
import datetime

import pydantic
from jmm import models

from handlers import common


class Payload(pydantic.BaseModel):
    period: int = pydantic.Field(alias="$period", default=60 * 60 * 24)
    issues: List[models.Issue] = pydantic.Field(alias="$issues", default=[])
    sprints: List[models.Sprint] = pydantic.Field(alias="$sprints", default=[])
    components: List[str] = None


class Body(pydantic.BaseModel):
    payload: Payload
    query: models.Query


async def handler(body: Body) -> Dict[str, List[models.Point]]:
    query = body.query

    payload = body.payload
    issues = payload.issues
    period = datetime.timedelta(seconds=payload.period)
    components = payload.components or []

    result = {}
    dt = query.range_.to_
    statuses = set([])
    while dt >= query.range_.from_:
        component__counter = {}
        for issue in issues:
            for change in issue.changes:
                if change.field == 'status':
                    statuses.add(change.value_to_string)

            if not common.is_done(issue, dt - period, dt):
                continue

            for component in issue.components:
                if component.name in components or not components:
                    component__counter.setdefault(component.name, 0)
                    component__counter[component.name] += 1

        for label, counter in component__counter.items():
            result.setdefault(label, []).append(
                models.Point(
                    value=counter,
                    timestamp=dt.timestamp(),
                )
            )

        dt -= period

    return result
