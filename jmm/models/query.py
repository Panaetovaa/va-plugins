import datetime
from typing import List

from pydantic import BaseModel, Field


class Range(BaseModel):
    from_: datetime.datetime = Field(None, alias="from")
    to_: datetime.datetime = Field(None, alias="to")


class Target(BaseModel):
    target: str
    payload: dict


class Query(BaseModel):
    range_: Range = Field(None, alias="range")
    targets: List[Target]
    interval_ms: int = Field(None, alias="intervalMs")
    max_data_points: int = Field(None, alias="maxDataPoints")
    datetimes: List[datetime.datetime] | None = None


class Point(BaseModel):
    value: float
    timestamp: float
