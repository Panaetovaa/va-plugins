import datetime

from typing import List, Optional

from pydantic import BaseModel
from jmm.models.statuses import Status


class Sprint(BaseModel):
    id: int
    name: str
    start_date: datetime.datetime
    end_date: datetime.datetime
    complete_date: datetime.datetime
    activated_date: datetime.datetime
    goal: str
    board_names: List[str]
    board_ids: List[int]
    issues: List[str]


class User(BaseModel):
    key: str
    display_name: str
    name: str
    email: str
    active: bool


class Component(BaseModel):
    id: str
    name: str


class IssueType(BaseModel):
    name: str
    description: str
    subtask: bool


class IssueLink(BaseModel):
    type: str
    issue: str


class Change(BaseModel):
    issue: str
    author: User
    created: datetime.datetime
    value_from: Optional[str] = None
    value_from_string: Optional[str] = None
    value_to: Optional[str] = None
    value_to_string: Optional[str] = None
    field: str


class Issue(BaseModel):
    key: str
    created: datetime.datetime
    updated: datetime.datetime

    status: Status

    issue_type: IssueType

    assignee: Optional[User] = None
    creator: User
    reporter: User

    components: List[Component]
    links: List[IssueLink]

    subtasks: List[str]
    summary: str
    description: str

    changes: List[Change]
