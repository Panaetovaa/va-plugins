from fastapi import FastAPI

import handlers


app = FastAPI(debug=True)


app.post('/agile/closed_issues')(
    handlers.agile.closed_issues.handler
)
